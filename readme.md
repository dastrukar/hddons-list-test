> The CSS being used on this website comes from [Hideous Destructor HQ](https://gitlab.com/accensi/hdportal)

## What?
This is an experimental addons list fetcher thing.

The idea is to use a seperate repository to store the addons list. Ideally, each addon has its own "entry".
The entry will contain details about the mod itself.
All entries will then be processed by GitLab CI into a list of entries.
This list of entries will then be read by the website, becoming an addon entry on the website.

The whole point of this is to, hopefully, allow other people to easily update their own addon details via PRs/MRs, without having to interfere with the website stuff.
Also, you don't have to manually sort mods alphabetically, because the code does it all :p

The limitations from this current system are (at least from what i can tell): 
- You can't sort addons manually. (they are sorted alphabetically)
- It's a bit more slower than just pure HTML. (even more slower if your internet sucks ass)
- Custom links (like preview links) can only be placed at the end of an entry.
- You need at least know how to write some YAML syntax. (well, it ain't that hard tbh)

You win some, and you lose some, I guess.

[Here's the repository that's being used.](https://gitlab.com/dastrukar/addon-cdn-test)


*yes, this is copied from the page itself.*
*that's because i wrote it there first.*
