const Flags = {
	pinned: 1,
	new: 2,
};
const BASE_LINK = "https://gitlab.com/api/v4/projects/27912107/repository/files";
const END_LINK = "/raw?ref=main";

// Images
const GIT_ICON = "https://accensi.gitlab.io/hdportal/img/git.png";
const ZDOOM_ICON = "https://accensi.gitlab.io/hdportal/img/zdoom.png";
const DIRECT_ICON = "https://accensi.gitlab.io/hdportal/img/direct.png";

// Builds all the addon entries. prefix: The subdirectory
function buildEntries(prefix) {
	let xhr = new XMLHttpRequest();
	let page = BASE_LINK + "/" + prefix + "_list.json" + END_LINK;
	console.log("Attempting to get entries from " + page);

	xhr.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			let file = JSON.parse(this.responseText);

			// Last updated
			let date = document.createElement("div");
			date.classList.add("last-updated");
			date.innerHTML = "Last Updated: " + file.last_updated;

			let target = document.getElementsByClassName("about")[0];
			target.insertAdjacentElement("beforeend", date);

			for (let entry in file.list) {
				let e = file.list[entry];
				console.log(e);

				addEntry(
					e.title,
					e.credits,
					e.flairs,
					e.description,
					e.flag,
					e.links
				);
			}
		}
	}

	xhr.open("GET", page);
	xhr.send();
}

function addEntry(title, credits, flairs, desc, flag, links) {
	console.log("Adding entry: " + title);
	let section_id = "addons";

	// create a new addon
	let addon = document.createElement("section");
	addon.classList.add("block");

	// Check for flags
	if (flag) {
		addon.classList.add(flag);

		if (flag == "pinned") {
			section_id = "pinned";
		}
	}

	// Create the essentials
	let t = document.createElement("h3");
	let c = document.createElement("h5");
	let f = createFlairs(flairs);
	let d = document.createElement("p");
	let l = createLinks(links);

	// Add classes
	d.classList.add("description");

	// Set values
	t.innerHTML = title;
	c.innerHTML = credits;
	d.innerHTML = desc;

	// Append stuff
	console.log("Appending elements to addon entry...");
	addon.appendChild(t); // Title
	addon.appendChild(c); // Credits
	addon.appendChild(document.createElement("hr"));
	addon.appendChild(f); // Flairs
	addon.appendChild(d); // Description
	addon.appendChild(l); // Links

	// Add the entry
	let target = document.getElementById(section_id);
	target.insertAdjacentElement("beforeend", addon);
}

function createFlairs(flairs) {
	console.log("Creating flairs...");
	let fs = document.createElement("flairs");
	for (let f in flairs) {
		let div = document.createElement("div");
		console.log("Appending flair \"" + f + "\" with text: \"" + flairs[f] + "\"...");

		div.innerHTML = flairs[f];
		div.classList.add(f);

		fs.appendChild(div);
	}

	return fs;
}

function createLinks(links) {
	console.log("Creating links...");
	let lnks = document.createElement("links");

	// Process custom links (like preview links and what-not)
	// Do some hackery stuff to get the custom link to show up like how it should.
	let custom_links = document.createElement("p");
	custom_links.classList.add("description");

	for (let name in links.custom) {
		let l = links.custom[name];
		let a = document.createElement("a");
		console.log("Creating custom link \"" + l + "\" with name: " + name);

		a.innerHTML = name;
		a.href = l;

		custom_links.appendChild(a);
		custom_links.appendChild(document.createElement("br"));
	}
	lnks.appendChild(custom_links);

	for (let l in links.git) { lnks.appendChild(createIconLink(links.git[l], GIT_ICON)); }          // Process git/source code links
	for (let l in links.forum) { lnks.appendChild(createIconLink(links.forum[l], ZDOOM_ICON)); }    // Process forum links
	for (let l in links.direct) { lnks.appendChild(createIconLink(links.direct[l], DIRECT_ICON)); } // Process direct links

	return lnks;
}

function createIconLink(link, icon) {
	// Does it actually have a value
	if (link != undefined || link != "") {
		console.log("Creating iconlink \"" + link + "\" with icon: " + icon);
		let iconlink = document.createElement("iconlink");
		let a = document.createElement("a");
		let img = document.createElement("img");

		a.href = link;
		img.src = icon;

		img.classList.add("download");

		a.appendChild(img);
		iconlink.appendChild(a);
		iconlink.appendChild(document.createTextNode(" "));
		return iconlink;
	}
	return;
}
